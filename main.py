#!/bin/env python

"""
Interface module to be called from the frontend to manage Intergate 
processes.

This will use an API defined in ./api/api.py

Usage:

./main -a <action> -e <env> -s <server_name> -d [data_center] -p [processes] -i [ip]

- action: action to be executed. One of these: status, start, stop, restart.
- env: environment, which may be sit, tst or prod.
- server_name: name of the server where the action is going to be executed in.
- data_center: data center: either 'atl' or 'mia', without the quotes. It has 
    to be provided (mandatory) if 'env' is 'prod'.
- processes: name of processes affected by the action. To specify more than 
    one process, separate them with a colon (:).

This script will print the JSON result of this form if requested action 
was different than 'status':
{
    "error": "[error_message]",
    "result": "[result]"
}

If action was 'status', result JSON should be this form:
{
    "error": "[error_message]",
    "result": {
        "<server_name>": {
            "<process_name>": "0|1",
            ...
        },
        ...
    }
}

There is a mandatory conf.json file under ./conf/ which contains the 
environments and servers where the processes are going to be checked in.

Please note the final result from checking processes depends on the 
applications.ALL file under /home/weblogic in every server.
"""

import getopt
import json
from os import path
import sys

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'api.log')

sys.path.append(EXEC_DIR)
sys.path.append(path.dirname(EXEC_DIR))
from pssapi.logger import logger as lg
from api.api import IntegProcManagerAPI

logger = None
OPTIONS = "a:e:s:d:p:i:"
MANDATORY_OPTIONS = ['-a', '-e', '-s']
VALID_VALUES = {
    '-a': ['status', 'start', 'stop', 'restart'],
    '-e': ['sit', 'tst', 'prod'],
    '-d': ['atl', 'mia']
}

def _send_response(result):
    """Print result as a JSON content, and log any error."""
    if 'error' in result and result['error']:
        logger.error(result['error'])
    print(json.dumps(result))
    # print(result)

def _call_api(env, server, action, processes, data_center):
    """Call Intergate Processes Manager API and print result."""
    integprocAPI = IntegProcManagerAPI(env, server, processes, data_center)
    actions = {
        "status": integprocAPI.proc_status,
        "start": integprocAPI.start_proc,
        "stop": integprocAPI.stop_proc,
        "restart": integprocAPI.restart_proc
    }
    method2exe = actions.get(action, lambda: {'error': "Invalid API call."})
    _send_response(method2exe())

def _check_options(options):
    """Check passed options. Return a dictionary with option/value as 
    key/value.
    If any mandatory option is missing, or an unavailable value is provided 
    for an option, raise an exception.
    """
    parsed_options = {}
    for option in options:
        parsed_options[option[0]] = option[1]
    current_options = parsed_options.keys()
    if not set(MANDATORY_OPTIONS) <= set(current_options):
        raise Exception("Missing mandatory options.")
    valid_values_keys = VALID_VALUES.keys()
    for current_option in current_options:
        if current_option in valid_values_keys and \
           parsed_options[current_option] not in VALID_VALUES[current_option]:
            raise Exception("Value not allowed for option '{0}'".format(
                                                            current_option))
    if parsed_options['-e'] == 'prod' and '-d' not in parsed_options:
        raise Exception("Data center must be provided if environmet is PROD.")
    return parsed_options

def _main():
    global logger
    logger_fmtter = {
        'ERROR': '%(asctime)s - %(process)d: %(levelname)s: %(message)s',
        'DEFAULT': '%(asctime)s - %(process)d: %(message)s'
    }
    logger = lg.load_logger(log_file_name=LOG_FILE, cgi=True, **logger_fmtter)
    try:
        arguments = sys.argv[1:]
        logger.info(" ".join(arguments))
        options, dummy = getopt.gnu_getopt(arguments, OPTIONS)
        currrent_options = _check_options(options)
        action = currrent_options['-a']
        env = currrent_options['-e']
        server = currrent_options['-s']
        processes = currrent_options['-p'] \
                    if '-p' in currrent_options else ""
        data_center = currrent_options['-d'] \
                      if '-d' in currrent_options and env == 'prod' else ""
        _call_api(env, server, action, processes, data_center)
    except IndexError:
        _send_response({'error': "Missing parameters."})
    except KeyError:
        _send_response({'error': "Maybe specified server and env dont match?"})
    except Exception as e:
        import traceback
        # _send_response({'error': str(e)})
        _send_response({'error': traceback.format_exc()})

if __name__ == "__main__":
    _main()
