#!/bin/env python

"""
Script to be run remotely to check processes, and to execute actions over them.

Usage:

./_rmt.py <conf_file> <action> [processes]

-action: action to be executed on the server over the processes.
-processes: processes to apply the actions over. Can be any amount of them.
"""

from os import path
import subprocess
import sys
from threading import Lock
from time import sleep
from multiprocessing.pool import ThreadPool as Pool

ACTIONS2WAIT_FOR = ['status']
COMMAND_CAT_APPS_ALL = "cat {apps_dot_all_file}"
COMMAND_STATUS_PROC_V1 = "ps -efo pid,args | grep ' \\-D{proc} ' | grep -v grep"
COMMAND_STATUS_PROC_V2 = "ps -efo pid,args | grep ' \\-Dsname={proc} ' | grep -v grep"
# command to start process should include the 'cd' command because the 
# the script to start process includes Java libraries which resides in the 
# same directory
COMMAND_START_PROC = "cd {script_dir}; nohup ./{script} >{proc_nohup} 2>&1 &"
COMMAND_STOP_PROC = "kill -9 {pid}"

result = None
s_action_lock = None

def _update_result_data(process, data):
    """Update result global data so it can be returned back to the calling 
    script later.
    """
    result[process] = data

def _safe_action(process, data):
    """Thread safe for adding data to the global dict result."""
    with s_action_lock:
        result[process] = data

def _execute_command(command, wait=True):
    """Excute command and return the output."""
    if wait:
        p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = p.communicate()
        return output.strip(), error.strip()
    else:
        subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def _get_applications_all_file(conf_f):
    """Create and return a dictionary with the data of the applications.ALL 
    file which have the number '1'. Dictionary has this form:
    {
        <process_name>: <start_script>
    }
    """
    apps_all = {}
    cat_command = COMMAND_CAT_APPS_ALL.format(apps_dot_all_file=conf_f)
    stdoutput, stderror = _execute_command(cat_command)
    if stderror:
        raise Exception(stderror)
    # assuming applications.ALL file has this content:
    # <app_name>:<process_start_script>:<0|1>
    for line in stdoutput.split('\n'):
        _l = line.strip()
        if _l:
            line_data = line.split(':')
            if line_data[-1] == '1':
                apps_all[line_data[0]] = line_data[1]
    return apps_all

def _get_pid(result_ps_command):
    """Find and return the process id from the string passed in, which is the 
    result from executing a 'ps -efo pid,args' command.
    Return None if PID is not found.
    """
    res_list = result_ps_command.split(' ')
    for res_list_entry in res_list:
        # PID should be the first non-blank string found in the string
        possible_pid = res_list_entry.strip()
        if possible_pid:
            return possible_pid
    return None

def _proc_status(proc, dummy=None):
    """Find out whether proc is running or not.
    Return 1 if running. 0 otherwise.
    As there are 2 variants for running, 2 commands will be executed 
    to find out the string for the running process.
    """
    command = COMMAND_STATUS_PROC_V1.format(proc=proc)
    process_running1, dummy = _execute_command(command)
    if process_running1:
        return 1
    command = COMMAND_STATUS_PROC_V2.format(proc=proc)
    process_running2, dummy = _execute_command(command)
    return 1 if process_running2 else 0

def _is_process_up(process):
    """Return whether process is running or not."""
    return _proc_status(process) == 1

def _start_proc(proc, apps_all_dict):
    """Start proc up if it is not running."""
    if not _is_process_up(proc):
        proc_start_script = apps_all_dict[proc]
        base_path = path.dirname(proc_start_script)
        only_script = path.basename(proc_start_script)
        proc_nohup_file = path.join(base_path, 'nohup.out.{0}'.format(proc))
        command = COMMAND_START_PROC.format(script_dir=base_path, 
                                        script=only_script, 
                                        proc_nohup=proc_nohup_file)
        _execute_command(command, wait=False)

def _stop_proc(proc, dummy=None):
    """Stop proc if it is running."""
    if _is_process_up(proc):
        command = COMMAND_STATUS_PROC_V1.format(proc=proc)
        process_running, dummy = _execute_command(command)
        if not process_running:
            command = COMMAND_STATUS_PROC_V2.format(proc=proc)
            process_running, dummy = _execute_command(command)
        pid = _get_pid(process_running)
        if pid:
            # we have noticed some processes stay up even after executing 
            # the stop action once, so this is a hack for trying to kill it
            kill_attempts = 0
            while kill_attempts < 20:
                _execute_command(COMMAND_STOP_PROC.format(pid=pid), wait=False)
                if not _is_process_up(proc):
                    break
                kill_attempts += 1
                sleep(0.2)

def _restart_proc(proc, apps_all_dict):
    """Restart proc."""
    _stop_proc(proc)
    _start_proc(proc, apps_all_dict)

def _threaded_action(process, action_func, apps_all_dict):
    """Execute thread-safe action over the process and update the global 
    variable to save the result data.
    """
    proc_result = action_func(process, apps_all_dict)
    _safe_action(process, proc_result)

def _action2process(action, processes, apps_all_dict):
    """Execute the action over the process."""
    actions = {
        'status': _proc_status,
        'start': _start_proc,
        'stop': _stop_proc,
        'restart': _restart_proc
    }
    func2exe = actions.get(action, lambda: {'error': "Invalid function."})
    # wait for response only if dealing with one process, or 
    # the action to execute is 'status'
    if action in ACTIONS2WAIT_FOR:
        pool = Pool(len(processes))
        for process in processes:
            pool.apply_async(_threaded_action, (process, func2exe, apps_all_dict))
        pool.close()
        pool.join()
    else:
        for process in processes:
            func2exe(process, apps_all_dict)
        proc_result = "'{0}' command was sent to processes.".format(action)
        _update_result_data('bulk_message', proc_result)

def _main():
    global result
    result = {}
    global s_action_lock
    s_action_lock = Lock()
    try:
        arguments = sys.argv[1:]
        conf_file = arguments[0]
        action = arguments[1]
        apps_all_dict = _get_applications_all_file(conf_file)
        if len(arguments) > 2: # if at least one process is being passed in
            processes = arguments[2:]
        else: # no processes specified, which means all of them
            processes = apps_all_dict.keys()
        _action2process(action, processes, apps_all_dict)
        print(result)
    except Exception as e:
        print({'error': str(e)})

if __name__ == "__main__":
    _main()
