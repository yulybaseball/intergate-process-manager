"""
API for managing Intergate processes.

It connects and executes predefined actions over processes in provided 
servers.

Expose actions:

proc_status()
stop_proc()
start_proc()
restart_proc()

"""

import ast
from os import path
import sys

APP_DIR = path.dirname(path.dirname(path.realpath(__file__)))
CONF_FILE = path.join(APP_DIR, 'conf', 'conf.json')
RMT_EXEC_SCRIPT = path.join(APP_DIR, 'rmt', '_rmt.py')

from pssapi.cmdexe import cmd
from pssapi.utils import conf, util

def _execute_remote_script(apps_all_file, username, server, action, processes, 
                           wait=False):
    """Execute Python script remotely on server and return the output if 
    wait parameter is set to True.
    """
    arguments_python_rmt_script = [apps_all_file, action, processes]
    if wait:
        stdoutput, stderror = cmd.pythonexec(username, server, 
                                            RMT_EXEC_SCRIPT, wait, 
                                            *arguments_python_rmt_script)
        return stdoutput.strip(), stderror.strip()
    cmd.pythonexec(username, server, RMT_EXEC_SCRIPT, wait, 
                   *arguments_python_rmt_script)

def _server_data(env_conf, server, data_center):
    """Find username and location of the applications.ALL file for server.
    Return both: username, applications_all_file.
    """
    if data_center:
        apps_all_file = env_conf[data_center][server]['conf_file']
        username = env_conf[data_center][server]['username']
    else:
        apps_all_file = env_conf[server]['conf_file']
        username = env_conf[server]['username']
    return username, apps_all_file

class IntegProcManagerAPI:
    """Define the available and exposed actions to be executed over the 
    Intergate processes.
    """

    def __init__(self, env, server, processes, data_center):
        """Create the API.

        Keyword arguments:
        env - environment the server belongs to.
        server - server name to execute the action in.
        processes - string with the name of the processes to execute the 
            action to. If many processes were specified, they are split by 
            colon symbol (:). If no process was specified, this argument is 
            coming empty.
        data_center - data center the server belongs to.
        """
        env_conf = conf.get_conf(CONF_FILE)[env]
        username, apps_all_file = _server_data(env_conf, server, data_center)
        self.server = server
        self.processes = processes.replace(':', ' ')
        self.data_center = data_center
        self.username = username
        self.apps_all_file = apps_all_file

    def proc_status(self):
        """Check the status of procs in server.
        'procs' is a list of processes.
        Return a dict of this form:
        {
            "error": "[error_message]",
            "result": {
                "<server_name>": {
                    "<process_name>": "0|1",
                    ...
                },
                ...
            },
            "sorted_processes": [process_1, process_2, ..., process_n]
        }
        If procs is an empty list, this method will check the status of 
        every process.
        This method executes the action 'status' on server.
        """
        def _sort_processes(result):
            processes = result.keys()
            if 'error' in processes and len(processes) == 1:
                return []
            return util.sort_alphanumeric(processes)

        output, error = _execute_remote_script(self.apps_all_file, 
                                               self.username, self.server, 
                                               'status', self.processes, True)
        try:
            if output:
                dict_output = ast.literal_eval(output)
                return {
                    'result': dict_output,
                    'sorted_processes': _sort_processes(dict_output)
                }
        except Exception:
            pass
        return {'error': error}
        

    def start_proc(self):
        """Start procs (bring them up).
        'procs' is a list of processes.
        """
        _execute_remote_script(self.apps_all_file, self.username, self.server, 
                               'start', self.processes)
        return {'result':"'start' command was sent."}

    def stop_proc(self):
        """Stop procs (bring them down).
        'procs' is a list of processes.
        """
        _execute_remote_script(self.apps_all_file, self.username, self.server, 
                               'stop', self.processes)
        return {'result':"'stop' command was sent."}

    def restart_proc(self):
        """Restart procs (bring them down and up).
        'procs' is a list of processes.
        """
        _execute_remote_script(self.apps_all_file, self.username, self.server, 
                               'restart', self.processes)
        return {'result':"'restart' command was sent."}
