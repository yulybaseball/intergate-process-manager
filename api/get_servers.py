#!/bin/env python

"""Script to fetch the servers associated with the passed environment.
Will print a JSON-format response so it can used in the frontend.
Response has this form if everything went good:

{
    'servers': [server_1, server_2, server_3, ..., server_n]
}

Or this form if something went wrong:

{
    'error': <error_message>
}

Usage:

./get_servers.py <environment> [data_center]

environment: environment to search the servers in.
data_center: specific data center to search the servers in, for the passed 
    environment. It's mandatory if environment is 'prod'.

Please note this script relies all its job in the configuration file in the 
location ../conf/conf.json
"""

import json
from os import path
import sys

APP_DIR = path.dirname(path.dirname(path.realpath(__file__)))
CONF_FILE = path.join(APP_DIR, 'conf', 'conf.json')
ENV_WITH_DATA_CENTER = ['prod']

sys.path.append(path.dirname(APP_DIR))

from pssapi.utils import conf

def _print_response(response, error=False):
    """Print response to the front end in JSON format."""
    result = response if not error else {'error': response}
    # key = 'error' if error else 'servers'
    # result[key] = response
    print(json.dumps(result))

def _fetch_servers(servers_conf):
    """Fetch server names and server_type from servers_conf JSON.
    Return a dictionary of this form:
        {
            "servers": [<ordered_list_of_servers],
            "order_type": {
                <server_name>: <order_type>,
                ...
            }
        }
    """
    servers = {}
    servers['servers'] = sorted(servers_conf.keys())
    servers['server_type'] = {}
    for server in servers['servers']:
        servers['server_type'][server] = servers_conf[server]['server_type']
    return servers

def _main():
    try:
        params = sys.argv[1:]
        env = params[0]
        conf_data = conf.get_conf(CONF_FILE)
        if env in ENV_WITH_DATA_CENTER:
            data_center = params[1]
            servers = conf_data[env][data_center]
        else:
            servers = conf_data[env]
        # _print_response(sorted(servers.keys()))
        _print_response(_fetch_servers(servers))
    except IndexError:
        _print_response("Missing one parameter.", error=True)
    except KeyError:
        _print_response("JSON conf file is not well defined.", error=True)
    except Exception as e:
        _print_response(str(e), error=True)

if __name__ == "__main__":
    _main()
